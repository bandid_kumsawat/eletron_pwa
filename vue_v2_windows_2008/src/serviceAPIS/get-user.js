import API from '@/serviceAPIS/server-app-api'

const header = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
}

export default {
    getUser(){
        return API().get(
            '/user',
            header
        )
    },
    pushUser(data) {
        return API().post(
            '/user',
            data,
            header
        )
    }
}