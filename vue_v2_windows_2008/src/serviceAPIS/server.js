import axios from 'axios'

export default () => {
  var api = axios.create({
    baseURL: 'http://localhost:8080/'
  })
  return api
}
