import API from '@/serviceAPIS/server-app-api'
var config = {
    headers: {'Access-Control-Allow-Origin': '*'}
};
export default {
  flow () {
    return API().get('/red/flows/', config,  { useCredentails: true });
  },
  query () {
    return API().get('/g_query/', config, { useCredentails: true });
  },
  status () {
    return API().get('/dbcon/', config, { useCredentails: true } );
  }
}