import API from '@/serviceAPIS/server-app-api'

export default {
  getMap () {
    return API().get('/g_mapping/');
  },
  getTag () {
    return API().get('/scada_tag/');
  },
  updateMap (data) {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/g_mapping/", data, header)
  },
  getmapping (){
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/genquery/", header)
  }
}