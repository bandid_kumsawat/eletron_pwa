import Vue from 'vue'
import Router from 'vue-router'
// main scada
// main render
import render_view from '@/components/SCADA_SYNC/views/render_view.vue'
// ----------
import dbquery from '@/components/SCADA_SYNC/views/dbquery/main.vue'
import log from '@/components/SCADA_SYNC/views/log/main.vue'
import mapping from '@/components/SCADA_SYNC/views/mapping/main.vue'
import setting from '@/components/SCADA_SYNC/views/setting/main.vue'
import summary from '@/components/SCADA_SYNC/views/summary/main.vue'
import user from '@/components/SCADA_SYNC/views/manageUser/main.vue'
import NotFoundView from '@/components/SCADA_SYNC/views/NotFoundView.vue'

import login from '@/components/SCADA_SYNC/views/login/login.vue'
import logout from  '@/components/SCADA_SYNC/views/login/logout.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      // not found handler
      path: '*',
      name: 'NotFoundView',
      component: NotFoundView,
      meta: {description: 'NotFoundView.'}
    },
    {
      path: '/login',
      name: 'login',
      component: login,
    },
    {
      path: '/logout',
      name: 'logout',
      component: logout
    },
    {
      path: "/",
      component: render_view,
      children: [
        {
          path: 'dbquery',
          name: 'DB Query',
          component: dbquery,
          meta: {description: 'Databese Query.'}
        }, {
          path: 'log',
          name: 'log',
          component: log,
          meta: {description: 'Log.'}
        }, {
          path: 'mapping',
          name: 'mapping',
          component: mapping,
          meta: {description: 'mapping.'}
        }, {
          path: 'setting',
          name: 'setting',
          component: setting,
          meta: {description: 'setting.'}
        }, {
          path: 'summary',
          name: 'summary',
          alias: '/',
          // "router * "
          component: summary,
          meta: {description: 'summary.'}
        }, {
          path: 'user',
          name: 'user',
          component: user,
          meta: {description: 'summary.'}
        }
      ]
    },
  ]
})
