export default {
  user: null,
  token: null,
  type: null,
  email: null,
  scada_tag:{
    STATION: []
  },
  state_page: {
    default: [],
    dquery: {
      connection: {
        data: []
      },
      query: {
        data: []
      },
      result: {
        data: []
      }
    },
    log: {
      data1: 1,
      data2: 2,
      data3: 3,
      data4: 4
    },
    mapping: [],
    setting: [],
    summary: []
  },
  province: [],
  uploads: false  
}
