import state from "./state"

export default {
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_TYPE (state, type) {
    state.type = type
  },
  SET_EMAIL (state, email) {
    state.email = email
  },
  SET_RESULT (state, data) {
    state.state_page.dquery.result = data
  },
  SET_QUERY (state, data) {
    state.state_page.dquery.query.data = data
  },
  PUSH_QUERY(state, data){
    state.state_page.dquery.query.data.push(data)
  },
  MANY_PUSH_QUERY(state, data){
    state.state_page.dquery.query.data.forEach((item1, index1, arr1) => {
      data.new_tabs.forEach((item2, index2, arr2) => {
        console.log(item1.tabs_name === item2.tabs_name)
        if (item1.tabs_name === item2.tabs_name){
          state.state_page.dquery.query.data.splice(index1, 1)
        }
      })
    })
    console.log(state.state_page.dquery.query.data)
    data.new_tabs.forEach((item2, index2, arr2) => {
      state.state_page.dquery.query.data.push(item2)
    })
    console.log(state.state_page.dquery.query.data)
  },
  SET_SCADA_TAG (state, data) {
    state.scada_tag = data
  },
  SET_MAPPING (state, data){
    state.state_page.mapping = data
    },
  DELETE_MAPPING( state, data){
    state.state_page.mapping.splice(data.index_map,1)
  },
  UPLOAD_MAPPING_CSV_CLEAR ( state, index){
    state.state_page.mapping[index].data = []
  },
  UPLOAD_MAPPING_CSV( state, data ){
    state.state_page.mapping[data.index_map_csv].data.push({
      branch_code: data.branch_code,
      station_code: data.station_code,
      station_type: data.station_type,
      station_name: data.station_name,
      device_code: data.device_code,
      name: data.name,
      code: data.code,
      map: data.map,
      c: data.c,
      collect: data.collect,
      dup: data.dup,
      tag: data.tag,
      topic: data.topic,
    })
    // state.state_page.mapping[data.index_map_csv].branch_code = data.branch_code
    // state.state_page.mapping[data.index_map_csv].station_code = data.station_code
    // state.state_page.mapping[data.index_map_csv].station_type = data.station_type
    // state.state_page.mapping[data.index_map_csv].station_name = data.station_name
    // state.state_page.mapping[data.index_map_csv].device_code = data.device_code
    // state.state_page.mapping[data.index_map_csv].name = data.name
    // state.state_page.mapping[data.index_map_csv].code = data.code
    // state.state_page.mapping[data.index_map_csv].map = data.map

    // state.state_page.mapping[data.index_map_csv].c = data.c
    // state.state_page.mapping[data.index_map_csv].collect = data.collect
    // state.state_page.mapping[data.index_map_csv].dup = data.dup
    // state.state_page.mapping[data.index_map_csv].tag = data.tag
    // state.state_page.mapping[data.index_map_csv].topic = data.topic
  },
  UPDATE_MAPPING( state, data){
    state.state_page.mapping[data.index_map].code = data.data.code
    state.state_page.mapping[data.index_map].count = data.data.count
    state.state_page.mapping[data.index_map].name = data.data.name
    state.state_page.mapping[data.index_map].station_name = data.data.station_name
    state.state_page.mapping[data.index_map].sql_map = data.data.sql_map
    state.state_page.mapping[data.index_map].st_desc = data.data.st_desc
    state.state_page.mapping[data.index_map].type = data.data.type
    state.state_page.mapping[data.index_map].data.forEach((item, index, arr) => {
      item.station_code = data.data.code
      item.station_name = data.data.station_name
      item.station_type = data.data.type
      item.topic = 'SCADA/'+item.branch_code+'/'+item.station_code+'/'+item.station_type+'/'+item.station_name+'/'+item.name+'/'+item.device_code
    })
  },
  PUSH_MAPPING (state, data){
    state.state_page.mapping.push(data)
  },
  PUSH_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index].data.push(data.data)
  },
  UPDATE_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index_map].data[data.index_tag] = data.data
  },
  REMOVE_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index_map].data.splice(data.index_tag, 1)
  },
  SET_MAPPING_REMOVE(state, data,){
    state.state_page.mapping[data.index].data = data.data
  },
  PUSH_CONNECTION (state, data){
    state.state_page.dquery.connection.data.push(
      data
    )
  },
  UPDATE_CONNECTION (state, data) {
    state.state_page.dquery.connection.data[data.index_select].db_name = data.db_name
    state.state_page.dquery.connection.data[data.index_select].ip = data.ip
    state.state_page.dquery.connection.data[data.index_select].pass = data.pass
    state.state_page.dquery.connection.data[data.index_select].user = data.user
    state.state_page.dquery.connection.data[data.index_select].port = data.port
    state.state_page.dquery.connection.data[data.index_select].status = data.status
    state.state_page.dquery.connection.data[data.index_select].type = data.type
  },
  DELETE_CONNECTION (state, index){
    if (  state.state_page.dquery.connection.data[index].connection_name !== "MSSQL_CON1" && 
          state.state_page.dquery.connection.data[index].connection_name !== "mongo_con"
      ){
      state.state_page.dquery.connection.data.splice(index, 1)
    } else {
      alert("Can't remove Connection")
    }
  },
  // manage setting state
  SET_SETTING (state, data) {
    state.state_page.setting = data
  },
  SET_COMMONSETTING (state, data){
    state.state_page.commonsetting = data
  },
  SET_EMAILSCADA (state, data) {
    state.state_page.email = data
  },
  UPDATE_COMMONSETTING (state, data) {
    state.state_page.commonsetting.collect_data_time = data.collect_data_time
    state.state_page.commonsetting.collect_date = data.collect_date
    state.state_page.commonsetting.server = data.server
  },
  UPDATE_EMAILSCADA (state, data) {
    state.state_page.email.mail_send = data
  },
  SET_PROVINCE (state, data) {
    state.province = data
  },
  UPDATE_STATIONTYPE (state, data){
    console.log(state.state_page.setting[data.indexSetting].station[data.indexStation])
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.SEDIMENT = data.st_count.SEDIMENT
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.FILTATION = data.st_count.FILTATION
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.CHEMICAL = data.st_count.CHEMICAL
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.CLEARWELL = data.st_count.CLEARWELL
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.ELEVATE = data.st_count.ELEVATE
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.RWP = data.st_count.RWP
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.HSP = data.st_count.HSP
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.DPB = data.st_count.DPB
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.BOOSTER = data.st_count.BOOSTER
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.MB = data.st_count.MB
    // state.state_page.setting[data.indexSetting].station[data.indexStation].st_count.POINT = data.st_count.POINT
  },
  UPDATE_AREA (state, data){
    state.state_page.setting[data.indexSetting].reg = data.reg
    state.state_page.setting[data.indexSetting].reg_desc = data.reg_desc
  },
  UPDATE_BRANCH (state, data){
    state.state_page.setting[data.indexSetting].ww = data.ww
    state.state_page.setting[data.indexSetting].ww_desc = data.ww_desc
  },
  PUSH_STATION (state, data) {
    state.state_page.setting[data.indexSetting].station.push(data.station)
  },
  REMOVE_STATION (state, data){
    state.state_page.setting[data.indexSetting].station.splice(data.stationIndex, 1)
  },
  PUSH_SETTING(state, data){
    state.state_page.setting.push(data)
  },
  PUSH_STATION_TAG_TYPE_MULTI (state, data){
    // data :{
    //   indexSetting: 0,
    //   indexStation: 0,
    //   tag: '',
    //   type: ''
    // }
    state.state_page.setting[data.indexSetting].station[data.indexStation].stationType.forEach((item, index, arr) => {
      if (item.tag === data.tag){
        item.type = data.type
      }
    })
  },
  PUSH_STATION_TAG_TYPE(state, data){
    state.state_page.setting[data.indexSetting].station[data.indexStation].stationType.push(data.station_tag_type)
    state.state_page.setting[data.indexSetting].station[data.indexStation].st_count[data.station_tag_type.tag] = 0
  },
  REMOVE_QUERY(state, data){
    // var data = {
    //   tabs_name: ''
    // }
    state.state_page.dquery.query.data.forEach((item, index, arr) => {
      if (item.tabs_name === data.tabs_name){
        state.state_page.dquery.query.data.splice(index, 1)
      }
    })
  },
  SETDEFAULT(state, data){
    state.state_page.default = data
  },
  SETUPLOADS(state, logic){
    state.uploads = logic
  }
}
