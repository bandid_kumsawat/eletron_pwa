import API from '@/serviceAPIS/server-app-api'

const header = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
}

export default {
    query () {
        return API().get('/g_query/');
    },
    saveQuery (data) {
        return API().post("/g_query/", data, header)
    },
    sendQuery(db, sql){
        return API().post(
            '/query/' + db,
            sql,
            header
        )
    },
    saveUserlog (data){

        return API().post("/user/log", data, header) 

    },
    flow () {
        return API().get('/red/flows/', header,  { useCredentails: true } );
    },
    query () {
        return API().get('/g_query/', header, { useCredentails: true } );
    },
    status () {
        return API().get('/dbcon/', header, { useCredentails: true } );
    },
    area_branch () {
        return API().get('/province/', header, { useCredentails: true } );
    },
    station_info () {
        return API().get('/g_info/', header, { useCredentails: true } );
    },
    update_station_info (data) {
        return API().post('/g_info/' , data, header );
    },
    find_email() {
        return API().get('/email')
    },
    update_email(data) {
        console.log(data)
        return API().post('/email',data, header, { useCredentails: true })
    },
    // new api for v2.
    LIST_COMMONSETTING () {
        return API().get('/setting')
    },
    UPDATE_LIST_COMMONSETTING (data) {
        return API().post('/setting',data, header, { useCredentails: true })
    },
    LIST_INFO () {
        return API().get('/list_info')
    },
    LIST_DEFALUT_STATION() {
        return API().get('/default_conf')
    },
    LIST_INFO_BY_STATION (INDEX_STATION) {
        return API().get('/g_info/'+ (INDEX_STATION))
    },
    UPDATE_LIST_INFO_BY_STATION (INDEX_INFO, data){
        return API().post('/g_info/'+ (INDEX_INFO),data, header, { useCredentails: true })
    },
    REMOVE_LIST_INFO_BY_STATION (INDEX_INFO) {
        return API().delete('/g_info/'+ (INDEX_INFO))
    },
    PROVINCE () {
        return API().get('/province/', header, { useCredentails: true } );
    },
    GETUSER () {
        return API().get('/user', header, { useCredentails: true } );
    },
    SETUSER (data) {
        return API().post('/user',data, header, { useCredentails: true })
    },
}