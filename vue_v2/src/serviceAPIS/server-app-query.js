import axios from 'axios'

export default () => {
  var api = axios.create({
    /**baseURL: 'http://'+window.location.hostname+':8080/'*/
    baseURL: 'http://'+window.location.hostname+':1880/'
  })
  return api
}