import API from '@/serviceAPIS/server-app-api'

export default {
  getMap () {
    return API().get('/g_mapping/');
  },
  getTag () {
    return API().get('/scada_tag/');
  },
  updateMap (data) {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/g_mapping/", data, header)
  },
  getmapping (){
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/genquery/", header)
  },
  reload_db () {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/reload_db", header)
  },
  dowloads () {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/downloadmany/", header)
  },
  uploads (data) {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/uploadmany/",data, header)
  }
}