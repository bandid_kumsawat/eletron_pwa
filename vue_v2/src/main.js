// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'core-js/es6/symbol';
import 'core-js/es6/object';
import 'core-js/es6/function';
import 'core-js/es6/parse-int';
import 'core-js/es6/parse-float';
import 'core-js/es6/number';
import 'core-js/es6/math';
import 'core-js/es6/string';
import 'core-js/es6/date';
import 'core-js/es6/array';
import 'core-js/es6/regexp';
import 'core-js/es6/map';
import 'core-js/es6/set';

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import VueTableDynamic from 'vue-table-dynamic'
Vue.use(VueTableDynamic)
// import VueNativeSock from 'vue-native-websocket'

Vue.config.productionTip = false

sync(store, router)

if (window.localStorage) {
  var localUserString = window.localStorage.getItem('user') || 'null'
  var localUser = localUserString
  if (localUser && store.state.user !== localUser) {
    store.commit('SET_USER', localUser)
    store.commit('SET_TOKEN', window.localStorage.getItem('token'));
    store.commit('SET_EMAIL', window.localStorage.getItem('email'));
    store.commit('SET_TYPE', window.localStorage.getItem('type'))
  } else {
    router.push('/login')
  }
}


// Vue.use(VueNativeSock, 'ws://iotdma.info:1880/pwa/sensor',{
//   reconnection: true
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store: store,
})