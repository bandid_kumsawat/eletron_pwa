export default {
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_TYPE (state, type) {
    state.type = type
  },
  SET_EMAIL (state, email) {
    state.email = email
  },
  SET_RESULT (state, data) {
    state.state_page.dquery.result = data
  },
  SET_QUERY (state, data) {
    state.state_page.dquery.query.data = data
  },
  SET_SCADA_TAG (state, data) {
    state.scada_tag = data
  },
  SET_MAPPING (state, data){
    state.state_page.mapping = data
    },
  DELETE_MAPPING( state, data){
    state.state_page.mapping.splice(data.index_map,1)
  },
  UPDATE_MAPPING( state, data){
    state.state_page.mapping[data.index_map].code = data.data.code
    state.state_page.mapping[data.index_map].count = data.data.count
    state.state_page.mapping[data.index_map].name = data.data.name
    state.state_page.mapping[data.index_map].sql_map = data.data.sql_map
    state.state_page.mapping[data.index_map].st_desc = data.data.st_desc
    state.state_page.mapping[data.index_map].type = data.data.type
  },
  PUSH_MAPPING (state, data){
    state.state_page.mapping.push(data)
  },
  PUSH_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index].data.push(data.data)
  },
  UPDATE_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index_map].data[data.index_tag] = data.data
  },
  REMOVE_MAPPING_DATA (state, data){
    state.state_page.mapping[data.index_map].data.splice(data.index_tag, 1)
  }
}
