import Vue from 'vue'
import Router from 'vue-router'
import data_history from '@/components/DataHistory/data_history.vue'
import NotFoundView from '@/components/SCADA_SYNC/views/NotFoundView.vue'

import login from '@/components/SCADA_SYNC/views/login/login.vue'
import logout from  '@/components/SCADA_SYNC/views/login/logout.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      // not found handler
      path: '*',
      name: 'NotFoundView',
      component: NotFoundView,
      meta: {description: 'NotFoundView.'}
    },
    {
      path: '/login',
      name: 'login',
      component: login,
    },
    {
      path: '/logout',
      name: 'logout',
      component: logout
    },
    {
      path: "/",
      name: "data_history",
      component: data_history
    },
  ]
})
