import axios from 'axios'

export default () => {
  var api = axios.create({
    baseURL: 'http://pwaapi.iotdma.info/api'
  })
  return api
}
