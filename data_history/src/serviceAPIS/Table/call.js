import API from '@/serviceAPIS/Table/server'

export default {
  data (device) {
    return API().post('/scadasync/find/all', device)
    // return API().post('/QUERY_INFLUX', device)
  },
  influx (){
    return API().post('/QUERY_INFLUX')
  }
}
