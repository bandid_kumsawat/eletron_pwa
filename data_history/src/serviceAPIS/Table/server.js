import axios from 'axios'

export default () => {
  var api = axios.create({
    // baseURL: 'http://localhost:4000/api/'
    // baseURL: 'http://localhost:4000/api/'
    // baseURL: 'http://iotdma.info:1880/'
    // baseURL: 'http://dwdev.info:4000/api'
    baseURL: 'http://iotdma.info:1880'
  })
  return api
}
