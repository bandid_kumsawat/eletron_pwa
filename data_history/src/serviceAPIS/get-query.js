import API from '@/serviceAPIS/server-app-api'

export default {
  query () {
    return API().get('/g_query/');
  },
  squery (data) {
    var header = {
      'Content-Type': 'application/json'
    }
    return API().post("/g_query/", data, header)
  },
  send_query (db, data){
    var header = {
      'Content-Length': 0,
      'Content-Type': 'text/plain'
    }
    return API().post("/"+db , data, header)
  }
}
//   {
//     'Content-Type': 'application/json'
//   }, {
//     "pwa_info": {
//         "reg": "REG1",
//         "reg_desc": "กปภ.เขต 1",
//         "ww": "5531011",
//         "ww_desc": "สาขาชลบุรี(ชั้นพิเศษ)"
//     },
//     "station": [
//         {
//             "st_code": "BP1",
//             "st_desc": "บางพระ1",
//             "st_count": {
//                 "SEDIMENT": 0,
//                 "FILTATION": 0,
//                 "CHEMICAL": 0,
//                 "CLEARWELL": 0,
//                 "ELEVATE": 0,
//                 "RWP": 0,
//                 "HSP": 0,
//                 "DPB": 0,
//                 "BOOSTER": 0
//             }
//         }
//     ]
// }