import axios from 'axios'

export default () => {
  var api = axios.create({
    /**baseURL: 'http://'+window.location.hostname+':2880/'*/
    baseURL: 'http://'+window.location.hostname+'/'
  })
  return api
}