import API from '@/serviceAPIS/server-app-api'

const header = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
}

export default {
    query () {
        return API().get('/g_query/');
    },
    saveQuery (data) {
        return API().post("/g_query/", data, header)
    },
    sendQuery(db, sql){
        return API().post(
            '/' + db,
            sql,
            header
        )
    },
    saveUserlog (data){

        return API().post("/user/log", data, header) 

    },
    flow () {
        return API().get('/red/flows/', header,  { useCredentails: true } );
    },
    query () {
        return API().get('/g_query/', header, { useCredentails: true } );
    },
    status () {
        return API().get('/dbcon/', header, { useCredentails: true } );
    },
    area_branch () {
        return API().get('/province/', header, { useCredentails: true } );
    },
    station_info () {
        return API().get('/g_info/', header, { useCredentails: true } );
    },
    update_station_info (data) {
        return API().post('/g_info/' , data, header );
    },
    find_email() {
        return API().get('/email')
    },
    update_email(data) {
        console.log(data)
        return API().post('/email',data, header, { useCredentails: true })
    }
}