import API from '@/serviceAPIS/server-app-query'

const header = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
}

export default {
    sendQuery(db, sql){
        return API().post(
            '/' + db,
            sql,
            header
        )
    }
}