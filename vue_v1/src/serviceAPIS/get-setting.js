import API from '@/serviceAPIS/server-app-api'

export default {
  province () {
    return API().get("/province/")
  },
  getInfo () {
      return API().get("/g_info/")
  },
  updateInfo (data) {
        var header = {
        'Content-Type': 'application/json'
        }
        return API().post("/g_info/",data, header)
  }
}