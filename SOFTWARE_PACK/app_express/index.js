const express = require("express");
const http = require('http');
const cors = require('cors');
const os = require('os');
const fs = require('fs');
const url = require('url');
const path = require('path');
const bodyParser = require("body-parser");
const uds = require('underscore')
const axios = require('axios')
const fetch = require('node-fetch');
var red_app = express();



var listenPort = 8080


//CheckConfig
const configDir = 'C://pwa_sync_conf';
if (!fs.existsSync(configDir)) {
    fs.mkdirSync(configDir);
}

var info_config = {}

//InitConfig
const infoFile = configDir + '/info.json';
if (!fs.existsSync(infoFile)) {
    info_config =
        {
            "pwa_info": {
                "reg": "REG1",
                "reg_desc": "กปภ.เขต 1",
                "ww": "5531011",
                "ww_desc": "สาขาชลบุรี(ชั้นพิเศษ)",
                "collect_date": 7,
                "server": "164.115.62.63:8080",
                "collect_data_time": 7
            },
            "station": [
                {
                    "st_code": "BP1",
                    "st_desc": "บางพระ1",
                    "st_count": {
                        "SEDIMENT": 0,
                        "FILTATION": 0,
                        "CHEMICAL": 0,
                        "CLEARWELL": 0,
                        "ELEVATE": 0,
                        "RWP": 0,
                        "HSP": 0,
                        "DPB": 0,
                        "BOOSTER": 0
                    },
                    "pump": [

                    ]
                }
            ]
        }
    fs.writeFileSync(configDir + "/info.json", JSON.stringify(info_config));
} else {

    fs.readFile(configDir + "/info.json", 'utf8', (err, jsonString) => {
        if (err) {
            // console.log("File read failed:", err)
            return
        }
        info_config = JSON.parse(jsonString);

    })
}

// let raw_template2 = fs.readFileSync(configDir + "/template_table.json");
// let raw_template2 = fs.readFileSync(configDir + "/template_v3.json");
//JSON.parse(raw_template2);

//InitTemplate
const templateFile = configDir + '/template_v3.json';
var template2 = []
if (!fs.existsSync(templateFile)) {

    tmp_v3 = [
        {
            "GROUP": "CLEAR",
            "MAP": [
                {
                    "H1": "TANK",
                    "MAP": [
                        {
                            "H2": "LV",
                            "device_code": "TANK",
                            "code": "LEVEL"
                        },
                        {
                            "H2": "TYPE",
                            "device_code": "TANK",
                            "code": "TYPE"
                        },
                        {
                            "H2": "VOL",
                            "device_code": "TANK",
                            "code": "VOL"
                        }
                    ]
                }
            ]
        },
        {
            "GROUP": "ELEVATE",
            "MAP": [
                {
                    "H1": "LS",
                    "MAP": [
                        {
                            "H2": "H",
                            "device_code": "ELEVATE",
                            "name": "0",
                            "code": "LS_H"
                        },
                        {
                            "H2": "HH",
                            "device_code": "ELEVATE",
                            "name": "0",
                            "code": "LS_HH"
                        },
                        {
                            "H2": "L",
                            "device_code": "ELEVATE",
                            "name": "0",
                            "code": "LS_L"
                        },
                        {
                            "H2": "LL",
                            "device_code": "ELEVATE",
                            "name": "0",
                            "code": "LS_LL"
                        },
                        {
                            "H2": "M",
                            "device_code": "ELEVATE",
                            "name": "0",
                            "code": "LS_M"
                        }
                    ]
                },
                {
                    "H1": "LV",
                    "device_code": "ELEVATE",
                    "name": "0",
                    "code": "LEVEL"
                },
                {
                    "H1": "VOL",
                    "device_code": "ELEVATE",
                    "name": "0",
                    "code": "VOL"
                },
                {
                    "H1": "TYPE",
                    "device_code": "ELEVATE",
                    "name": "0",
                    "code": "TYPE"
                },
                {
                    "H1": "FLOW",
                    "device_code": "ELEVATE",
                    "name": "0",
                    "code": "FLOW",
                    "MAP": [
                        {
                            "H2": "STATUS",
                            "device_code": "ELEVATE",
                            "name": "FLOW",
                            "code": "STATUS"
                        }
                    ]
                }
            ]
        },
        {
            "GROUP": "FILTER",
            "MAP": [
                {
                    "H1": "TANK",
                    "MAP": [
                        {
                            "H2": "LV",
                            "device_code": "FILTER",
                            "name": "0",
                            "code": "LEVEL"
                        },
                        {
                            "H2": "VOL",
                            "device_code": "FILTER",
                            "name": "0",
                            "code": "VOL"
                        }
                    ]
                }
            ]
        },
        {
            "GROUP": "FLOW",
            "device_code": "FLOW",
            "code": "RATE",
            "MAP": [
                {
                    "H1": "TOTAL",
                    "device_code": "FLOW",
                    "code": "TOTAL"
                },
                {
                    "H1": "IN",
                    "device_code": "FLOWIN",
                    "code": "RATE",
                    "MAP": [
                        {
                            "H2": "TOTAL",
                            "device_code": "FLOWIN",
                            "code": "TOTAL"
                        }
                    ]
                }
            ]
        },
        {
            "GROUP": "PT",
            "device_code": "P1",
            "code": "RATE",
            "MAP": [
                {
                    "H1": "IN",
                    "device_code": "PTIN",
                    "code": "RATE"
                }
            ]
        },
        {
            "GROUP": "SEDIMENT",
            "MAP": [
                {
                    "H1": "LV",
                    "device_code": "SEDIMENT",
                    "code": "LEVEL"
                }
            ]
        },
        {
            "GROUP": "FILTER",
            "MAP": [
                {
                    "H1": "TANK",
                    "MAP": [
                        {
                            "H2": "LV",
                            "device_code": "FILTER",
                            "name": "0",
                            "code": "LEVEL"
                        },
                        {
                            "H2": "VOL",
                            "device_code": "FILTER",
                            "name": "0",
                            "code": "VOL"
                        }
                    ]
                }
            ]
        },
        {
            "GROUP": "RESOURCE",
            "MAP": [
                {
                    "H1": "LV",
                    "device_code": "RESOURCE",
                    "code": "LEVEL"
                },
                {
                    "H1": "VOL",
                    "device_code": "RESOURCE",
                    "code": "VOL"
                }
            ]
        },
        {
            "GROUP": "CL2",
            "device_code": "WQ",
            "code": "CL2"
        },
        {
            "GROUP": "COND",
            "device_code": "WQ",
            "code": "COND"
        },
        {
            "GROUP": "DO",
            "device_code": "WQ",
            "code": "DO"
        },
        {
            "GROUP": "HDM",
            "device_code": "WQ",
            "code": "HDM"
        },
        {
            "GROUP": "ORP",
            "device_code": "WQ",
            "code": "ORP"
        },
        {
            "GROUP": "PH",
            "device_code": "WQ",
            "code": "PH"
        },
        {
            "GROUP": "TEMP",
            "device_code": "WQ",
            "code": "TEMP"
        },
        {
            "GROUP": "TURBIT",
            "device_code": "WQ",
            "code": "TURBIT"
        },
        {
            "GROUP": "DataDateTime",
            "device_code": "0",
            "code": "TIME"
        }
    ]
    template2 = tmp_v3
    fs.writeFileSync(configDir + "/template_v3.json", JSON.stringify(tmp_v3));
} else {
    //    var raw_template2 = fs.readFileSync(configDir + "/template_v3.json");
    //    template2 = JSON.parse(raw_template2);
    fs.readFile(configDir + "/template_v3.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        template2 = JSON.parse(jsonString);
    })
}

//InitProvince
var province_data = []
const province_file = configDir + '/province.json';
if (!fs.existsSync(province_file)) {
    province = [
        {
            "reg": "REG1",
            "reg_desc": "กปภ.เขต1",
            "ww_all": [
                {
                    "ww": 5531011,
                    "ww_desc": "สาขาชลบุรี (ชั้นพิเศษ)"
                },
                {
                    "ww": 5531012,
                    "ww_desc": "สาขาพัทยา(ชั้นพิเศษ)"
                },
                {
                    "ww": 5531013,
                    "ww_desc": "สาขาบ้านบึง"
                },
                {
                    "ww": 5531014,
                    "ww_desc": "สาขาพนัสนิคม"
                },
                {
                    "ww": 5531015,
                    "ww_desc": "สาขาศรีราชา"
                },
                {
                    "ww": 5531016,
                    "ww_desc": "สาขาแหลมฉบัง"
                },
                {
                    "ww": 5531017,
                    "ww_desc": "สาขาฉะเชิงเทรา"
                },
                {
                    "ww": 5531018,
                    "ww_desc": "สาขาบางปะกง"
                },
                {
                    "ww": 5531019,
                    "ww_desc": "สาขาบางคล้า"
                },
                {
                    "ww": 5531020,
                    "ww_desc": "สาขาพนมสารคาม"
                },
                {
                    "ww": 5531021,
                    "ww_desc": "สาขาระยอง"
                },
                {
                    "ww": 5531022,
                    "ww_desc": "สาขาบ้านฉาง"
                },
                {
                    "ww": 5531023,
                    "ww_desc": "สาขาปากน้ำประแสร์"
                },
                {
                    "ww": 5531024,
                    "ww_desc": "สาขาจันทบุรี"
                },
                {
                    "ww": 5531025,
                    "ww_desc": "สาขาขลุง"
                },
                {
                    "ww": 5531026,
                    "ww_desc": "สาขาตราด"
                },
                {
                    "ww": 5531027,
                    "ww_desc": "สาขาคลองใหญ่"
                },
                {
                    "ww": 5531028,
                    "ww_desc": "สาขาสระแก้ว"
                },
                {
                    "ww": 5531029,
                    "ww_desc": "สาขาวัฒนานคร"
                },
                {
                    "ww": 5531030,
                    "ww_desc": "สาขาอรัญประเทศ"
                },
                {
                    "ww": 5531031,
                    "ww_desc": "สาขาปราจีนบุรี"
                },
                {
                    "ww": 5531032,
                    "ww_desc": "สาขากบินทร์บุรี"
                }
            ]
        },
        {
            "reg": "REG2",
            "reg_desc": "กปภ.เขต2",
            "ww_all": [
                {
                    "ww": 5541011,
                    "ww_desc": "สาขาพระพุทธบาท"
                },
                {
                    "ww": 5541012,
                    "ww_desc": "สาขาหนองแค"
                },
                {
                    "ww": 5541013,
                    "ww_desc": "สาขามวกเหล็ก"
                },
                {
                    "ww": 5541014,
                    "ww_desc": "สาขาบ้านหมอ"
                },
                {
                    "ww": 5541015,
                    "ww_desc": "สาขาลพบุรี"
                },
                {
                    "ww": 5541016,
                    "ww_desc": "สาขาบ้านหมี่"
                },
                {
                    "ww": 5541017,
                    "ww_desc": "สาขาชัยบาดาล"
                },
                {
                    "ww": 5541018,
                    "ww_desc": "สาขาสิงห์บุรี"
                },
                {
                    "ww": 5541019,
                    "ww_desc": "สาขาอ่างทอง"
                },
                {
                    "ww": 5541020,
                    "ww_desc": "สาขาวิเศษชัยชาญ"
                },
                {
                    "ww": 5541021,
                    "ww_desc": "สาขาพระนครศรีอยุธยา"
                },
                {
                    "ww": 5541022,
                    "ww_desc": "สาขาผักไห่"
                },
                {
                    "ww": 5541023,
                    "ww_desc": "สาขาเสนา"
                },
                {
                    "ww": 5541024,
                    "ww_desc": "สาขาท่าเรือ"
                },
                {
                    "ww": 5541025,
                    "ww_desc": "สาขาปทุมธานี"
                },
                {
                    "ww": 5541026,
                    "ww_desc": "สาขารังสิต (ชั้นพิเศษ)"
                },
                {
                    "ww": 5541027,
                    "ww_desc": "สาขานครนายก"
                },
                {
                    "ww": 5541028,
                    "ww_desc": "สาขาบ้านนา"
                },
                {
                    "ww": 5541029,
                    "ww_desc": "สาขานครราชสีมา"
                },
                {
                    "ww": 5541030,
                    "ww_desc": "สาขาปากช่อง"
                },
                {
                    "ww": 5541031,
                    "ww_desc": "สาขาครบุรี"
                },
                {
                    "ww": 5541032,
                    "ww_desc": "สาขาสีคิ้ว"
                },
                {
                    "ww": 5541033,
                    "ww_desc": "สาขาปักธงชัย"
                },
                {
                    "ww": 5541034,
                    "ww_desc": "สาขาโชคชัย"
                },
                {
                    "ww": 5541035,
                    "ww_desc": "สาขาพิมาย"
                },
                {
                    "ww": 5541036,
                    "ww_desc": "สาขาชุมพวง"
                },
                {
                    "ww": 5541037,
                    "ww_desc": "สาขาโนนสูง"
                },
                {
                    "ww": 5541038,
                    "ww_desc": "สาขาด่านขุนทด"
                },
                {
                    "ww": 5541039,
                    "ww_desc": "สาขาคลองหลวง"
                },
                {
                    "ww": 5541040,
                    "ww_desc": "สาขาธัญบุรี"
                }
            ]
        },
        {
            "reg": "REG3",
            "reg_desc": "กปภ.เขต3",
            "ww_all": [
                {
                    "ww": 5542011,
                    "ww_desc": "สาขาบ้านโป่ง"
                },
                {
                    "ww": 5542012,
                    "ww_desc": "สาขาสวนผึ้ง"
                },
                {
                    "ww": 5542013,
                    "ww_desc": "สาขาปากท่อ"
                },
                {
                    "ww": 5542014,
                    "ww_desc": "สาขาราชบุรี"
                },
                {
                    "ww": 5542015,
                    "ww_desc": "สาขาสมุทรสงคราม"
                },
                {
                    "ww": 5542016,
                    "ww_desc": "สาขาสมุทรสาคร"
                },
                {
                    "ww": 5542017,
                    "ww_desc": "สาขาอ้อมน้อย"
                },
                {
                    "ww": 5542018,
                    "ww_desc": "สาขาสามพราน"
                },
                {
                    "ww": 5542019,
                    "ww_desc": "สาขาสุพรรณบุรี"
                },
                {
                    "ww": 5542020,
                    "ww_desc": "สาขาศรีประจันต์"
                },
                {
                    "ww": 5542021,
                    "ww_desc": "สาขาเดิมบางนางบวช"
                },
                {
                    "ww": 5542022,
                    "ww_desc": "สาขาด่านช้าง"
                },
                {
                    "ww": 5542023,
                    "ww_desc": "สาขาอู่ทอง"
                },
                {
                    "ww": 5542024,
                    "ww_desc": "สาขากาญจนบุรี"
                },
                {
                    "ww": 5542025,
                    "ww_desc": "สาขาเลาขวัญ"
                },
                {
                    "ww": 5542026,
                    "ww_desc": "สาขาพนมทวน"
                },
                {
                    "ww": 5542027,
                    "ww_desc": "สาขาท่ามะกา"
                },
                {
                    "ww": 5542028,
                    "ww_desc": "สาขาเพชรบุรี"
                },
                {
                    "ww": 5542029,
                    "ww_desc": "สาขาประจวบคีรีขันธ์"
                },
                {
                    "ww": 5542030,
                    "ww_desc": "สาขาปราณบุรี"
                },
                {
                    "ww": 5542031,
                    "ww_desc": "สาขากุยบุรี"
                },
                {
                    "ww": 5542032,
                    "ww_desc": "สาขาบางสะพาน"
                },
                {
                    "ww": 5542033,
                    "ww_desc": "สาขานครปฐม"
                }
            ]
        },
        {
            "reg": "REG4",
            "reg_desc": "กปภ.เขต4",
            "ww_all": [
                {
                    "ww": 5551011,
                    "ww_desc": "สาขาสุราษฎร์ธานี"
                },
                {
                    "ww": 5551012,
                    "ww_desc": "สาขากาญจนดิษฐ์"
                },
                {
                    "ww": 5551013,
                    "ww_desc": "สาขาเกาะสมุย"
                },
                {
                    "ww": 5551014,
                    "ww_desc": "สาขาบ้านนาสาร"
                },
                {
                    "ww": 5551015,
                    "ww_desc": "สาขาบ้านตาขุน"
                },
                {
                    "ww": 5551016,
                    "ww_desc": "สาขาไชยา"
                },
                {
                    "ww": 5551017,
                    "ww_desc": "สาขาชุมพร"
                },
                {
                    "ww": 5551018,
                    "ww_desc": "สาขาหลังสวน"
                },
                {
                    "ww": 5551019,
                    "ww_desc": "สาขาท่าแซะ"
                },
                {
                    "ww": 5551020,
                    "ww_desc": "สาขาระนอง"
                },
                {
                    "ww": 5551021,
                    "ww_desc": "สาขาพังงา"
                },
                {
                    "ww": 5551022,
                    "ww_desc": "สาขาตะกั่วป่า"
                },
                {
                    "ww": 5551023,
                    "ww_desc": "สาขาท้ายเหมือง"
                },
                {
                    "ww": 5551024,
                    "ww_desc": "สาขาภูเก็ต"
                },
                {
                    "ww": 5551025,
                    "ww_desc": "สาขากระบี่"
                },
                {
                    "ww": 5551026,
                    "ww_desc": "สาขาอ่าวลึก"
                },
                {
                    "ww": 5551027,
                    "ww_desc": "สาขาทุ่งสง"
                },
                {
                    "ww": 5551028,
                    "ww_desc": "สาขาชะอวด"
                },
                {
                    "ww": 5551029,
                    "ww_desc": "สาขาปากพนัง"
                },
                {
                    "ww": 5551030,
                    "ww_desc": "สาขาจันดี"
                },
                {
                    "ww": 5551031,
                    "ww_desc": "สาขาขนอม"
                },
                {
                    "ww": 5551032,
                    "ww_desc": "สาขานครศรีธรรมราช"
                },
                {
                    "ww": 5551033,
                    "ww_desc": "สาขาเกาะพะงัน"
                },
                {
                    "ww": 5551034,
                    "ww_desc": "สาขาคลองท่อม"
                }
            ]
        },
        {
            "reg": "REG5",
            "reg_desc": "กปภ.เขต5",
            "ww_all": [
                {
                    "ww": 5552011,
                    "ww_desc": "สาขาสงขลา"
                },
                {
                    "ww": 5552012,
                    "ww_desc": "สาขาหาดใหญ่"
                },
                {
                    "ww": 5552013,
                    "ww_desc": "สาขาสะเดา"
                },
                {
                    "ww": 5552014,
                    "ww_desc": "สาขานาทวี"
                },
                {
                    "ww": 5552015,
                    "ww_desc": "สาขาระโนด"
                },
                {
                    "ww": 5552016,
                    "ww_desc": "สาขาพัทลุง"
                },
                {
                    "ww": 5552017,
                    "ww_desc": "สาขาเขาชัยสน"
                },
                {
                    "ww": 5552018,
                    "ww_desc": "สาขาตรัง"
                },
                {
                    "ww": 5552019,
                    "ww_desc": "สาขาห้วยยอด"
                },
                {
                    "ww": 5552020,
                    "ww_desc": "สาขาย่านตาขาว"
                },
                {
                    "ww": 5552021,
                    "ww_desc": "สาขากันตัง"
                },
                {
                    "ww": 5552022,
                    "ww_desc": "สาขาสตูล"
                },
                {
                    "ww": 5552023,
                    "ww_desc": "สาขาละงู"
                },
                {
                    "ww": 5552024,
                    "ww_desc": "สาขายะหา"
                },
                {
                    "ww": 5552025,
                    "ww_desc": "สาขาเบตง"
                },
                {
                    "ww": 5552026,
                    "ww_desc": "สาขาสายบุรี"
                },
                {
                    "ww": 5552027,
                    "ww_desc": "สาขานราธิวาส"
                },
                {
                    "ww": 5552028,
                    "ww_desc": "สาขารือเสาะ"
                },
                {
                    "ww": 5552029,
                    "ww_desc": "สาขาสุไหงโกลก"
                },
                {
                    "ww": 5552030,
                    "ww_desc": "สาขาพังลา"
                }
            ]
        },
        {
            "reg": "REG6",
            "reg_desc": "กปภ.เขต6",
            "ww_all": [
                {
                    "ww": 5521011,
                    "ww_desc": "สาขา (ชั้นพิเศษ) สาขาขอนแก่น"
                },
                {
                    "ww": 5521012,
                    "ww_desc": "สาขาบ้านไผ่"
                },
                {
                    "ww": 5521013,
                    "ww_desc": "สาขาชุมแพ"
                },
                {
                    "ww": 5521014,
                    "ww_desc": "สาขาน้ำพอง"
                },
                {
                    "ww": 5521015,
                    "ww_desc": "สาขาชนบท"
                },
                {
                    "ww": 5521016,
                    "ww_desc": "สาขากระนวน"
                },
                {
                    "ww": 5521017,
                    "ww_desc": "สาขาหนองเรือ"
                },
                {
                    "ww": 5521018,
                    "ww_desc": "สาขาเมืองพล"
                },
                {
                    "ww": 5521019,
                    "ww_desc": "สาขากาฬสินธุ์"
                },
                {
                    "ww": 5521020,
                    "ww_desc": "สาขากุฉินารายณ์"
                },
                {
                    "ww": 5521021,
                    "ww_desc": "สาขาสมเด็จ"
                },
                {
                    "ww": 5521022,
                    "ww_desc": "สาขามหาสารคาม"
                },
                {
                    "ww": 5521023,
                    "ww_desc": "สาขาพยัคฆภูมิพิสัย"
                },
                {
                    "ww": 5521024,
                    "ww_desc": "สาขาชัยภูมิ"
                },
                {
                    "ww": 5521025,
                    "ww_desc": "สาขาแก้งคร้อ"
                },
                {
                    "ww": 5521026,
                    "ww_desc": "สาขาจัตุรัส"
                },
                {
                    "ww": 5521027,
                    "ww_desc": "สาขาหนองบัวแดง"
                },
                {
                    "ww": 5521028,
                    "ww_desc": "สาขาภูเขียว"
                },
                {
                    "ww": 5521029,
                    "ww_desc": "สาขาบำเหน็จณรงค์"
                },
                {
                    "ww": 5521030,
                    "ww_desc": "สาขาร้อยเอ็ด"
                },
                {
                    "ww": 5521031,
                    "ww_desc": "สาขาโพนทอง"
                },
                {
                    "ww": 5521032,
                    "ww_desc": "สาขาสุวรรณภูมิ"
                }
            ]
        },
        {
            "reg": "REG7",
            "reg_desc": "กปภ.เขต7",
            "ww_all": [
                {
                    "ww": 5522011,
                    "ww_desc": "สาขาอุดรธานี"
                },
                {
                    "ww": 5522012,
                    "ww_desc": "สาขากุมภวาปี"
                },
                {
                    "ww": 5522013,
                    "ww_desc": "สาขาบ้านผือ"
                },
                {
                    "ww": 5522014,
                    "ww_desc": "สาขาบ้านดุง"
                },
                {
                    "ww": 5522015,
                    "ww_desc": "สาขาหนองบัวลำภู"
                },
                {
                    "ww": 5522016,
                    "ww_desc": "สาขาเลย"
                },
                {
                    "ww": 5522017,
                    "ww_desc": "สาขาเชียงคาน"
                },
                {
                    "ww": 5522018,
                    "ww_desc": "สาขาด่านซ้าย"
                },
                {
                    "ww": 5522019,
                    "ww_desc": "สาขาวังสะพุง"
                },
                {
                    "ww": 5522020,
                    "ww_desc": "สาขาหนองคาย"
                },
                {
                    "ww": 5522021,
                    "ww_desc": "สาขาบึงกาฬ"
                },
                {
                    "ww": 5522022,
                    "ww_desc": "สาขาศรีเชียงใหม่"
                },
                {
                    "ww": 5522023,
                    "ww_desc": "สาขาโพนพิสัย"
                },
                {
                    "ww": 5522024,
                    "ww_desc": "สาขาสกลนคร"
                },
                {
                    "ww": 5522025,
                    "ww_desc": "สาขาสว่างแดนดิน"
                },
                {
                    "ww": 5522026,
                    "ww_desc": "สาขาพังโคน"
                },
                {
                    "ww": 5522027,
                    "ww_desc": "สาขานครพนม"
                },
                {
                    "ww": 5522028,
                    "ww_desc": "สาขาธาตุพนม"
                },
                {
                    "ww": 5522029,
                    "ww_desc": "สาขาบ้านแพง"
                },
                {
                    "ww": 5522030,
                    "ww_desc": "สาขาศรีสงคราม"
                }
            ]
        },
        {
            "reg": "REG8",
            "reg_desc": "กปภ.เขต8",
            "ww_all": [
                {
                    "ww": 5532011,
                    "ww_desc": "สาขาอุบลราชธานี"
                },
                {
                    "ww": 5532012,
                    "ww_desc": "สาขาพิบูลมังสาหาร"
                },
                {
                    "ww": 5532013,
                    "ww_desc": "สาขาเดชอุดม"
                },
                {
                    "ww": 5532014,
                    "ww_desc": "สาขาเขมราฐ"
                },
                {
                    "ww": 5532015,
                    "ww_desc": "สาขาอำนาจเจริญ"
                },
                {
                    "ww": 5532016,
                    "ww_desc": "สาขายโสธร"
                },
                {
                    "ww": 5532017,
                    "ww_desc": "สาขาเลิงนกทา"
                },
                {
                    "ww": 5532018,
                    "ww_desc": "สาขามหาชนะชัย"
                },
                {
                    "ww": 5532019,
                    "ww_desc": "สาขาบุรีรัมย์"
                },
                {
                    "ww": 5532020,
                    "ww_desc": "สาขาสตึก"
                },
                {
                    "ww": 5532021,
                    "ww_desc": "สาขาลำปลายมาศ"
                },
                {
                    "ww": 5532022,
                    "ww_desc": "สาขานางรอง"
                },
                {
                    "ww": 5532023,
                    "ww_desc": "สาขาละหานทราย"
                },
                {
                    "ww": 5532024,
                    "ww_desc": "สาขาสุรินทร์"
                },
                {
                    "ww": 5532025,
                    "ww_desc": "สาขาศีขรภูมิ"
                },
                {
                    "ww": 5532026,
                    "ww_desc": "สาขารัตนบุรี"
                },
                {
                    "ww": 5532027,
                    "ww_desc": "สาขาสังขะ"
                },
                {
                    "ww": 5532028,
                    "ww_desc": "สาขาศรีสะเกษ"
                },
                {
                    "ww": 5532029,
                    "ww_desc": "สาขากันทรลักษ์"
                },
                {
                    "ww": 5532030,
                    "ww_desc": "สาขามุกดาหาร"
                }
            ]
        },
        {
            "reg": "REG9",
            "reg_desc": "กปภ.เขต9",
            "ww_all": [
                {
                    "ww": 5511011,
                    "ww_desc": "สาขาเชียงใหม่(ชั้นพิเศษ)"
                },
                {
                    "ww": 5511012,
                    "ww_desc": "สาขาฮอด"
                },
                {
                    "ww": 5511013,
                    "ww_desc": "สาขาสันกำแพง"
                },
                {
                    "ww": 5511014,
                    "ww_desc": "สาขาแม่ริม"
                },
                {
                    "ww": 5511015,
                    "ww_desc": "สาขาแม่แตง"
                },
                {
                    "ww": 5511016,
                    "ww_desc": "สาขาฝาง"
                },
                {
                    "ww": 5511017,
                    "ww_desc": "สาขาจอมทอง"
                },
                {
                    "ww": 5511018,
                    "ww_desc": "สาขาแม่ฮ่องสอน"
                },
                {
                    "ww": 5511019,
                    "ww_desc": "สาขาแม่สะเรียง"
                },
                {
                    "ww": 5511020,
                    "ww_desc": "สาขาลำพูน"
                },
                {
                    "ww": 5511021,
                    "ww_desc": "สาขาบ้านโฮ่ง"
                },
                {
                    "ww": 5511022,
                    "ww_desc": "สาขาลำปาง"
                },
                {
                    "ww": 5511023,
                    "ww_desc": "สาขาเกาะคา"
                },
                {
                    "ww": 5511024,
                    "ww_desc": "สาขาเถิน"
                },
                {
                    "ww": 5511025,
                    "ww_desc": "สาขาแพร่"
                },
                {
                    "ww": 5511026,
                    "ww_desc": "สาขาเด่นชัย"
                },
                {
                    "ww": 5511027,
                    "ww_desc": "สาขาร้องกวาง"
                },
                {
                    "ww": 5511028,
                    "ww_desc": "สาขาน่าน"
                },
                {
                    "ww": 5511029,
                    "ww_desc": "สาขาท่าวังผา"
                },
                {
                    "ww": 5511030,
                    "ww_desc": "สาขาพะเยา"
                },
                {
                    "ww": 5511031,
                    "ww_desc": "สาขาจุน"
                },
                {
                    "ww": 5511032,
                    "ww_desc": "สาขาเชียงราย"
                },
                {
                    "ww": 5511033,
                    "ww_desc": "สาขาพาน"
                },
                {
                    "ww": 5511034,
                    "ww_desc": "สาขาเทิง"
                },
                {
                    "ww": 5511035,
                    "ww_desc": "สาขาเวียงเชียงของ"
                },
                {
                    "ww": 5511036,
                    "ww_desc": "สาขาแม่สาย"
                },
                {
                    "ww": 5511037,
                    "ww_desc": "สาขาแม่ขะจาน"
                }
            ]
        },
        {
            "reg": "REG10",
            "reg_desc": "กปภ.เขต10",
            "ww_all": [
                {
                    "ww": 5512011,
                    "ww_desc": "สาขานครสวรรค์"
                },
                {
                    "ww": 5512012,
                    "ww_desc": "สาขาท่าตะโก"
                },
                {
                    "ww": 5512013,
                    "ww_desc": "สาขาลาดยาว"
                },
                {
                    "ww": 5512014,
                    "ww_desc": "สาขาพยุหะคีรี"
                },
                {
                    "ww": 5512015,
                    "ww_desc": "สาขาชัยนาท"
                },
                {
                    "ww": 5512016,
                    "ww_desc": "สาขาอุทัยธานี"
                },
                {
                    "ww": 5512017,
                    "ww_desc": "สาขากำแพงเพชร"
                },
                {
                    "ww": 5512018,
                    "ww_desc": "สาขาขาณุวรลักษณบุรี"
                },
                {
                    "ww": 5512019,
                    "ww_desc": "สาขาตาก"
                },
                {
                    "ww": 5512020,
                    "ww_desc": "สาขาแม่สอด"
                },
                {
                    "ww": 5512021,
                    "ww_desc": "สาขาสุโขทัย"
                },
                {
                    "ww": 5512022,
                    "ww_desc": "สาขาทุ่งเสลี่ยม"
                },
                {
                    "ww": 5512023,
                    "ww_desc": "สาขาศรีสำโรง"
                },
                {
                    "ww": 5512024,
                    "ww_desc": "สาขาสวรรคโลก"
                },
                {
                    "ww": 5512025,
                    "ww_desc": "สาขาศรีสัชนาลัย"
                },
                {
                    "ww": 5512026,
                    "ww_desc": "สาขาอุตรดิตถ์"
                },
                {
                    "ww": 5512027,
                    "ww_desc": "สาขาพิษณุโลก"
                },
                {
                    "ww": 5512028,
                    "ww_desc": "สาขานครไทย"
                },
                {
                    "ww": 5512029,
                    "ww_desc": "สาขาพิจิตร"
                },
                {
                    "ww": 5512030,
                    "ww_desc": "สาขาบางมูลนาก"
                },
                {
                    "ww": 5512031,
                    "ww_desc": "สาขาตะพานหิน"
                },
                {
                    "ww": 5512032,
                    "ww_desc": "สาขาเพชรบูรณ์"
                },
                {
                    "ww": 5512033,
                    "ww_desc": "สาขาหล่มสัก"
                },
                {
                    "ww": 5512034,
                    "ww_desc": "สาขาชนแดน"
                },
                {
                    "ww": 5512035,
                    "ww_desc": "สาขาหนองไผ่"
                },
                {
                    "ww": 5512036,
                    "ww_desc": "สาขาวิเชียรบุรี"
                }
            ]
        }
    ]
    fs.writeFileSync(configDir + "/province.json", JSON.stringify(province));
} else {
    fs.readFile(configDir + "/province.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        province_data = JSON.parse(jsonString);;
    })
}

// Init scada_tag
var scada_tag = {}

const scada_tag_file = configDir + '/scada_tag.json';
if (!fs.existsSync(scada_tag_file)) {
    scada_tag = {
        "STATION": [
            {
                "tag": "SEDIMENT",
                "st_desc": "ถังตกตะกอน"
            },
            {
                "tag": "FILTATION",
                "st_desc": "โรงกรองน้ำ"
            },
            {
                "tag": "CHEMICAL",
                "st_desc": "โรงจ่ายสารเคมี"
            },
            {
                "tag": "CLEARWELL",
                "st_desc": "ถังน้ำใส"
            },
            {
                "tag": "ELEVATE",
                "st_desc": "หอถังสูง"
            },
            {
                "tag": "RWP",
                "st_desc": "โรงสูบน้ำแรงต่ำ"
            },
            {
                "tag": "HSP",
                "st_desc": "โรงสูบน้ำแรงสูง"
            },
            {
                "tag": "DPB",
                "st_desc": "สถานีจ่ายน้ำ"
            },
            {
                "tag": "BOOSTER",
                "st_desc": "สถานีเพิ่มแรงดัน"
            }
        ]
    }
    fs.writeFileSync(configDir + "/scada_tag.json", JSON.stringify(scada_tag));
} else {
    fs.readFile(configDir + "/scada_tag.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        scada_tag = JSON.parse(jsonString);;
    })
}


//InitQuery
const queryFile = configDir + '/query.json';
var query_config = [];
if (!fs.existsSync(queryFile)) {

    fs.writeFileSync(configDir + "/query.json", JSON.stringify(query_config));
} else {
    fs.readFile(configDir + "/query.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        query_config = JSON.parse(jsonString);;
    })
}


//InitMapping
var map_config = [];
const mapFile = configDir + '/mapping.json';
if (!fs.existsSync(mapFile)) {

    fs.writeFileSync(configDir + "/mapping.json", JSON.stringify(map_config));
} else {
    fs.readFile(configDir + "/mapping.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        map_config = JSON.parse(jsonString);
    })
}

//InitEmail
const emailFile = configDir + '/email.json';
var email_json = {}
if (!fs.existsSync(emailFile)) {

    var email_json = { "mail_send": ["suravut.kik@gmail.com"], "lost_time": 60 }
    fs.writeFileSync(configDir + "/email.json", JSON.stringify(email_json));
} else {
    fs.readFile(configDir + "/email.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        email_json = JSON.parse(jsonString);;
    })
}

// Init dbcon.json
const dbconFile = configDir + '/dbcon.json';
var dbcon = { "mssql1": false, "mssql2": false, "mssql3": false, "mssql4": false, "mongo": false, "pgsql1": false, "pgsql2": false, "influx": false, "con": [{ "connection_name": "MSSQL_CON1", "db_name": "PWA_CH", "ip": "127.0.0.1", "port": 1443, "type": "MICROSOFT_DB", "user": "sa", "pass": "12345678", "status": false }, { "connection_name": "MSSQL_CON2", "db_name": "PWA_CHONBURIx", "ip": "127.0.0.1", "port": 1443, "type": "MICROSOFT_DB", "user": "sa", "pass": "12345678", "status": false }, { "connection_name": "mongo_con", "db_name": "pwa", "ip": "127.0.0.1", "port": 27017, "type": "MONGO_DB", "user": "", "pass": "", "status": false }] }

if (!fs.existsSync(dbconFile)) {
    fs.writeFileSync(configDir + "/dbcon.json", JSON.stringify(dbcon));
} else {
    fs.readFile(configDir + "/dbcon.json", 'utf8', (err, jsonString) => {
        if (err) {
            return
        }
        dbcon = JSON.parse(jsonString);;
    })
}





red_app.use(bodyParser.json({ limit: '200mb' }));
// Add a simple route for static content served from 'public'
red_app.use(bodyParser.urlencoded({ parameterLimit: 100000, limit: '100mb', extended: true }));
red_app.use(cors());
red_app.use(express.static(__dirname + "/public"));


red_app.get('/email', (req, res) => {
    return res.json(email_json);
});

red_app.post('/email', (req, res) => {
    try {
        email_json = req.body
        fs.writeFile(configDir + "/email.json", JSON.stringify(email_json), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "email_json Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "email_json Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});



red_app.get('/g_info', (req, res) => {
    return res.json(info_config)
});
red_app.post('/g_info', (req, res) => {
    try {
        info_config = req.body
        fs.writeFile(configDir + "/info.json", JSON.stringify(info_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "info_config Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "info_config Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});




red_app.get('/province', (req, res) => {
    return res.json(province_data)
});


// API  g_query
red_app.get('/g_query', (req, res) => {
    return res.json(query_config)
});
red_app.post('/g_query', (req, res) => {

    try {
        query_config = req.body
        fs.writeFile(configDir + "/query.json", JSON.stringify(query_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "query Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "query Saved!" });
                console.error(`Got error: ${e.message}`);
            });
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
});


// API  g_mapping
red_app.get('/g_mapping', (req, res) => {
    return res.json(map_config)
});

red_app.post('/g_mapping', (req, res) => {
    //console.log(req.body)
    try {
        map_config = req.body
        fs.writeFile(configDir + "/mapping.json", JSON.stringify(map_config), function (err) {
            if (err) {
                console.log(err)
            };

            http.get('http://localhost:1880/fileupdate', (res2) => {
                console.log("update nodered ok")

                return res.json({ ret: "mapping Saved!" });
                // });
            }).on('error', (e) => {
                return res.json({ ret: "mapping Saved!" });
                console.error(`Got error: ${e.message}`);
            });

        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }

});


red_app.get('/scada_tag', (req, res) => {
    return res.json(scada_tag)
});

red_app.get('/dbcon', (req, res) => {
    // console.log(req)
    try {
        fs.readFile(configDir + "/dbcon.json", 'utf8', (err, jsonString) => {
            if (err) {

            }
            dbcon = JSON.parse(jsonString);
            return res.json(dbcon);
        })
    } catch (error) {
        return res.json(dbcon);
    }



});


//==================================
//       Gen From DB Section      
//==================================

function mapQuery (req_query,data){
    console.log("\r\n\r\n||==== Map Query ====||");
    // console.log(req_query.code);
    console.log(req_query.sql);
    console.log(req_query.tbl);
    console.log("||===================||");

    var ret_arr=[]
    var gg = uds.groupBy(data, function(num){ return num.Device; });
    
    console.log(Object.keys(gg).length);
    var keys = Object.keys(gg);
    console.log(keys);

    for(var x =0;x < keys.length ;x++){
        var qr_name = keys[x].split('_');

        var tmp = {

            "query_name": "",
            "table": "dbo."+req_query.tbl,
            "type": "MSSQL",
            "dbcon": "MSSQL_DB1",
            "logtable": req_query.tbl,
            "query": "",
            "tabs_name":""
        }

        if(qr_name.length <=3){
            tmp.query_name = qr_name[1] + "_" + qr_name[2]
            tmp.tabs_name  = qr_name[1] + "_" + qr_name[2]
            
        }else{

            tmp.query_name = qr_name[1] + "_" + qr_name[2] +"_"+ qr_name[3]
            tmp.tabs_name  = qr_name[1] + "_" + qr_name[2] +"_"+ qr_name[3]
        }
        tmp.tabs_name = tmp.tabs_name.replace('-','_');
        tmp.query_name = tmp.query_name.replace('-','_');

        switch(req_query.tbl){
            case "DATATBL_DPB":
                tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_DPB WHERE Device LIKE '%_" + qr_name[1] + "_%' ORDER BY DataDateTime DESC;"
                // SELECT TOP(1) * from dbo.DATATBL_PUMP WHERE Device LIKE '%_KS_%' ORDER BY DataDateTime DESC;
                break;

            case "DATATBL_RWP":
                tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_RWP WHERE Device LIKE '%_" + qr_name[1]+ "_%' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_POWER":
                tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_POWER WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] +"_"+ qr_name[3] +"%' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_PUMP":
                tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_PUMP WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] +"_"+ qr_name[3] + "%' ORDER BY DataDateTime DESC;"
                break;
            case "DATATBL_WQ":
                if(qr_name.length <=3){
                    tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_WQ WHERE Device LIKE '%_" + qr_name[1] + "_%' ORDER BY DataDateTime DESC;"
                }else{
                    tmp.query="SELECT TOP(1) * FROM dbo.DATATBL_WQ WHERE Device LIKE '%_" + qr_name[1] + "_" + qr_name[2] +"_"+ qr_name[3] + "%' ORDER BY DataDateTime DESC;"
                }
                break;
                
        }

        ret_arr.push(tmp)
    }
    //console.log(ret_arr);
    
    return ret_arr;
   
}


function loopQuery(req_query, station, pwa_info) {

    //const req_url = '127.0.0.1:8080/MSSQL_DB1'
    console.log(req_query.length)
    var loop_arr = [];
    var loop_arr_map = [];


    for (var j = 0; j < req_query.length; j++) {
        // for(var j=0;j< 2;j++){

        try {
            console.log(req_query[j].sql)
            fetch('http://localhost:1880/MSSQL_DB1', {
                method: 'post',
                body: JSON.stringify({
                    sql: req_query[j].sql
                }),
                headers: { 'Content-Type': 'application/json' }
            }).then(res => res.json())
                .then(data => {

                    console.log(data.length)

                });
            // let data = await response.json()
            // //console.log(data)
            // if(data.length > 0){
            //     // Add Query 
            //     var bbb = await mapQuery(req_query[j],data)
            //     var map = await mapMaping(req_query[j],data,station,pwa_info)
            //     loop_arr = loop_arr.concat(bbb);
            //     loop_arr_map = loop_arr_map.concat(map)
            // }





        } catch (error) {
            console.error(error);
        }
    }
    console.log("\r\n\r\n=======================================\r\n!!!!! End Query Map !!!!!")
    // console.log(loop_arr)
    // console.log(loop_arr.length)
    // fs.writeFileSync(configDir + "/query.json", JSON.stringify(loop_arr));
    // fs.writeFileSync(configDir + "/mapping.json", JSON.stringify(loop_arr_map));
    // return {ret:"ok "+loop_arr.length}

}


red_app.post('/genquery', (req, res) => {
    console.log("start");
    //const station = req.body.station;  
    //const pwa_info = req.body.pwa_info;  

    const station = info_config.station;
    const pwa_info = info_config.pwa_info;

    var search_query = []
    var loop_qry_arr=[]


    var search_cmd_1 = { sql: "SELECT TOP(100) * from dbo.DATATBL_DPB ORDER BY DataDateTime ASC;", tbl: "DATATBL_DPB" }
    var search_cmd_2 = { sql: "SELECT TOP(100) * from dbo.DATATBL_RWP ORDER BY DataDateTime ASC;", tbl: "DATATBL_RWP" }
    var search_cmd_3 = { sql: "SELECT TOP(100) * from dbo.DATATBL_POWER ORDER BY DataDateTime ASC;", tbl: "DATATBL_POWER" }
    var search_cmd_4 = { sql: "SELECT TOP(100) * from dbo.DATATBL_PUMP ORDER BY DataDateTime ASC;", tbl: "DATATBL_PUMP" }
    var search_cmd_5 = { sql: "SELECT TOP(100) * from dbo.DATATBL_WQ ORDER BY DataDateTime ASC;", tbl: "DATATBL_WQ" }

    search_query.push(search_cmd_1);
    search_query.push(search_cmd_2);
    search_query.push(search_cmd_3);
    search_query.push(search_cmd_4);
    search_query.push(search_cmd_5);
    console.log(search_query);
    console.log(search_query.length);


    // No Async Await  T_T HEll_BEGIN

    // DATATBL_DPB
    fetch('http://localhost:1880/MSSQL_DB1', {
        method: 'post',
        body: JSON.stringify({
            sql: search_cmd_1.sql
        }),
        headers: { 'Content-Type': 'application/json' }
    }).then(res => res.json())
      .then(data => {
            
            var DPB = mapQuery(search_cmd_1,data)
            loop_qry_arr = loop_qry_arr.concat(DPB);
            console.log(DPB)


        // DATATBL_RWP   
        fetch('http://localhost:1880/MSSQL_DB1', {
                method: 'post',
                body: JSON.stringify({
                    sql: search_cmd_2.sql
                }),
                headers: { 'Content-Type': 'application/json' }
            })
            .then(res => res.json())
            .then(data => {
                var RWP = mapQuery(search_cmd_2,data)
                loop_qry_arr = loop_qry_arr.concat(RWP);
                console.log(RWP)

                
                // DATATBL_POWER  
                fetch('http://localhost:1880/MSSQL_DB1', {
                    method: 'post',
                    body: JSON.stringify({
                        sql: search_cmd_3.sql
                    }),
                    headers: { 'Content-Type': 'application/json' }
                })
                .then(res => res.json())
                .then(data => {
                    var POWER = mapQuery(search_cmd_3,data)
                    loop_qry_arr = loop_qry_arr.concat(POWER);
                    console.log(POWER)


                    // DATATBL_PUMP 
                    fetch('http://localhost:1880/MSSQL_DB1', {
                        method: 'post',
                        body: JSON.stringify({
                            sql: search_cmd_4.sql
                        }),
                        headers: { 'Content-Type': 'application/json' }
                    })
                    .then(res => res.json())
                    .then(data => {
                        var PUMP = mapQuery(search_cmd_4,data)
                        loop_qry_arr = loop_qry_arr.concat(PUMP);
                        console.log(PUMP)

                        
                        // DATATBL_PUMP 
                        fetch('http://localhost:1880/MSSQL_DB1', {
                            method: 'post',
                            body: JSON.stringify({
                                sql: search_cmd_5.sql
                            }),
                            headers: { 'Content-Type': 'application/json' }
                        })
                        .then(res => res.json())
                        .then(data => {
                            var WQ = mapQuery(search_cmd_5,data)
                            loop_qry_arr = loop_qry_arr.concat(WQ);
                            console.log(WQ)

                            //Endding 
                            fs.writeFileSync(configDir + "/query.json", JSON.stringify(loop_qry_arr));
                        
                        });//H5
                    
                    });//H4

                })//H3


            })//H2


    })//H1
            






    // var ret =  loopQuery(search_query,station,pwa_info)
    // http.get('http://localhost:8080/fileupdate', (res) => {
    //     // const { statusCode } = res;
    // });
    // return res.json(data)




    // console.log(input_query.sql)
    // fetch('http://localhost:1880/MSSQL_DB1',{
    //     method: 'post',
    //     body: JSON.stringify({
    //         sql:input_query.sql
    //     }),
    //     headers: {'Content-Type': 'application/json'}
    //   }).then(res => res.json())
    //     .then(data => {

    //     console.log(data.length)
    //     return data;
    // });


    //     return new Promise(resolve => {
    //     setTimeout(() => resolve( fetch('http://localhost:1880/MSSQL_DB1',{
    //             method: 'post',
    //             body: JSON.stringify({
    //                 sql:input_query.sql
    //             }),
    //             headers: {'Content-Type': 'application/json'}
    //           }).then(res => res.json())
    //             .then(data => {

    //             console.log(data.length)
    //             return data;
    //         })), 10);
    //     });
    // };

    // const addresses = ['A', 'B', 'C', 'D', 'E', 'F'];

    // const promises = search_query.map(input_query => queryAPI(input_query));
    // console.log(addresses);
    // Promise.all(promises).then(results => {
    //     console.log(results);
    // });




});













// Create a server
var server = http.createServer(red_app);
server.listen(listenPort, '0.0.0.0');


